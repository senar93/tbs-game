﻿using UnityEngine;
using System.Collections;





public class ScriptGenericAbility : MonoBehaviour {

	[SerializeField] public LinkedEffect linkedScript;		//puntatore allo script contenente gli effetti dell abilità

	[SerializeField] protected EffectType effect;
	[SerializeField] protected EffectTagType effectTag;
	//[SerializeField] protected EffectTagType[] optional = null;
	[SerializeField] protected int priority = 0; //effetti con priorità maggiore vengono eseguiti prima

	[SerializeField] protected bool passive;

	[SerializeField] protected bool haveResourceCost;
	[SerializeField] protected ResourceCost ResourceCostScript;

	[SerializeField] protected bool disabling;
	[SerializeField] protected bool dispelling;
	[SerializeField] protected bool selectable; /*
	 * false	--> solo effetti particolari che puntano sempre ecomunque direttamente a questa abilità possono disabilitarla
	 * 				es. un abilità che infligge n danni da fuoco quando l'unità attacca o subisce un colpo viene implementata cosi:
	 * 				1. abilità che infligge n danni da fuoco quando l'unità attacca "selectable" = false
	 * 				2. abilità che [...] viene attacca "selectable" = false
	 * 				3. abilità che non fa nulla, ma se viene rimossa o disabilitata rimuove o disabilità le abilità 1 e 2, "selectable" = true
	 */



	#region Effect
	public float Value(){
		if (linkedScript != null)
			return linkedScript.Value();
		else
			return 0;
	}

	public void ApplyEffect(GameObject target){
		if (linkedScript != null)
			linkedScript.ApplyEffect (target);
	}
	public void ApplyEffect(GameObject[] target){
		if (linkedScript != null)
			linkedScript.ApplyEffect (target);
	}
	public void ApplyEffect(TacticMapNode target){
		if (linkedScript != null)
			linkedScript.ApplyEffect (target);
	}
	public void ApplyEffect(TacticMapNode[] target){
		if (linkedScript != null)
			linkedScript.ApplyEffect (target);
	}
	#endregion
	

	#region Get
	public bool IsPassive(){ return passive; }
	public EffectType Get_EffectType() { return effect; }
	public EffectTagType Get_EffectTagType() { return effectTag; }
	public int Get_priority(){ return priority; }
	public bool CanBeDispel(){ return dispelling; }
	public bool CanBeDisable(){ return disabling; }
	public bool CanBeSelected(){ return selectable; }

	#region Resource Cost
	public ResourceCostType[] Get_ResourceCostType(){ 
		if (haveResourceCost)
			return ResourceCostScript.GetComponent<ResourceCost> ().Get_ResourceCostType ();
		else
			return null;
	}
	public float[] Get_ResourceCostValue(){ 
		if (haveResourceCost)
			return ResourceCostScript.GetComponent<ResourceCost> ().Get_ResourceCostValue ();
		else
			return null;
	}
	#endregion

	#endregion


	public void Start(){
		//Debug.Log (linkedScript.Value ());
		//Debug.Log (this.gameObject.GetComponent<ScriptGenericAbility>());
	}

}
