﻿using UnityEngine;
using System.Collections;

public class ScriptAbilityResourceCost : ResourceCost {
	public ResourceCostType[] resourceType {get; private set;}
	public float[] cost {get; private set;}

	public override ResourceCostType[] Get_ResourceCostType(){
		return resourceType;
	}

	public override float[] Get_ResourceCostValue(){
		return cost;
	}

}
