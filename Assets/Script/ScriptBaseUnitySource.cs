﻿using UnityEngine;
using System.Collections;

public class ScriptBaseUnitySource : MonoBehaviour {

	public int initiative;

	public int health;

	public int mana;
	public float manaRegen;
	public int stamina;
	public float staminaRegen;

	public int armor;
	public ArmorType armorType;

	public int attackDamage;
	public AttackType attackType;
	public DamageType damageType;
	public int attackMaxRange;
	public int attackCostInitiative;

	public int movementMaxRange;
	public MovementType movementType;
	public int movementCost;

	public GameObject[] abilityList;

}
