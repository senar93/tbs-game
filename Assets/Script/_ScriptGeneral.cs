﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using System;

/*
 * NOTE
 * normalmente i modificatori fissi vengono applicati prima di quelli in percentuale
 * 
 * 
 */
/*
 * SINTASSI
 * - funzioni, classi e metodi iniziano con la lettera maiuscola (es. CustomShort)
 * - le variabili iniziano con la lettera minuscola (es. health)
 * - costanti e static sono scritti completamente in maiuscolo (es. MIN_HEALTH)
 * - metodi, variabili ecc devono avere nomi che rimandano alla loro funzione, in particolare:
 * 		- i metodi di una classe che sono utilizzabili da altre classi e leggono un valore iniziano con Get_ ,
 * 			quelli che impostano un valore con Set_ e quelli che lo modificano con Mod_
 * 
 * - enum, classi static e altre cose generali per l'intero programma vengono dichiarate dentro _ScriptGeneral,
 * 
 */



#region ENUM
public enum ArmorType {
	none = 0
}


//il tipo di danno che influisce sull armatura
public enum DamageType {
	none = 0
}


//il tipo di attacco, ranged, melee, ecc
public enum AttackType {
	none = 0,
	
	melee,
	renged
}


public enum MovementType {
	none = 0,
	
	normal,
	fly
}

public enum NodeType {
	none = 0,

	ground,
	mountain,
	water
}

public enum DirectionsType {
	leftUp = 0,
	left = 1,
	leftDown = 2,
	rightDown = 3,
	right = 4,
	rightUp = 5
}

public enum ShapeType {
	line = 0,
	ring,
	circle,

	//non ancora implementate
	cone
}

public enum ResourceCostType {
	none = 0,
	initiative,
	mana,
	stamina,
	health
}

public enum EffectType {
	custom = 0,
	value,
	doSomethingToTarget
}

public enum EffectTagType {
	customScript = 0,

	#region DamageRow
	attackingUnityNumber,
	attackType,
	damageType,
	// Damage
	baseDamage,
	damageInc,
	damageIncXC,
	totalDamage,

	#endregion

	#region DamageSend
	// Health
	baseHealth,
	healthInc,
	healthIncXC,
	effectiveHealth,
	// Armor
	baseArmor,
	armorInc,
	armorIncXC,
	armorTypeInc,
	armorContribution,
	// Final Calculate
	inflictDamage,
	deathUnityNumber,
	finalDamage,
	lastUnityHp,
	// After damage calc
	unityNumberReduction,
	onDeath
	#endregion

	//calcolo movimento

	//[...]
}
#endregion

#region Static Class
public static class CustomSort {
	
	public static int InsertionSort<T> (List<T> iniziative) where T : IComparable{
		T tmpValue;
		int j;
		int n = 0;
		
		for (int i = 1; i < iniziative.Count; i++) {
			tmpValue = iniziative[i];
			j = i - 1;
			while(j >= 0 && iniziative[j].CompareTo(tmpValue) > 0){
				iniziative[j + 1] = iniziative[j];
				j--;
				n++;
			}
			iniziative[j+1] = tmpValue;
		}
		
		return n;
	}
	// DA IMPLEMENTARE
	//overload per confrontare parametri di oggetti
	public static int InsertionSort (List<GameObject> iniziative, string scriptName) {
		GameObject tmpValue;
		int j;
		int n = 0;
		if (CustomCheck.AllNotNull(iniziative)) {
			if (scriptName == "ScriptBattleGroup") {
				//inserire qui l'ordinamento dei gruppi in base all iniziativa
				//modificando iniziative[j].CompareTo(tmpValue)

				/*
				for (int i = 1; i < iniziative.Count; i++) {
					tmpValue = iniziative[i];
					j = i - 1;
					while(j >= 0 && iniziative[j].CompareTo(tmpValue) > 0){
						iniziative[j + 1] = iniziative[j];
						j--;
						n++;
					}
					iniziative[j+1] = tmpValue;
				}*/

				//Debug.Log ("asd");
			}
		}
		return n;
	}


}



public static class CustomCheck {

	public static bool AllNotNull(List<GameObject> list){
		for (int i = 0; i < list.Count; i++)
			if(list[i] == null)
				return false;

		return true;
	}
	public static bool AllNotNull(GameObject[] array){
		for (int i = 0; i < array.Length; i++)
			if(array[i] == null)
				return false;
		
		return true;
	}

}



public static class CustomList<T> {

	public static List<T> Clone(List<T> baseList){
		List<T> tmpList = new List<T>(baseList.Count);
		for (int i = 0; i < baseList.Count; i++)
			tmpList [i] = baseList [i];

		return tmpList;
	}

}



public static class CustomAbilityList{
	//controlla che un elemento abbia almeno 1 dei tag
	public static bool HaveTag(GameObject ability, EffectTagType tag){
		if (ability.GetComponent<ScriptGenericAbility> ().Get_EffectTagType () == tag )
			return true;

		return false;
	}
	public static bool HaveTag(GameObject ability, EffectTagType[] tag){
		for (int i = 0; i < tag.Length; i++)
			if (HaveTag(ability,tag[i]))
				return true;

		return false;
	}
	//controlla che un elemento abbia tutti i tag
	public static bool HaveTagAll(GameObject ability, EffectTagType[] tag){
		for (int i = 0; i < tag.Length; i++)
			if( !HaveTag(ability,tag[i]) )
				return false;

		return true;
	}

	// se il valore di ritorno è -1 la lista non contiene elementi che soddisfino uno dei tag
	public static int FirstInList(List<GameObject> ability, EffectTagType[] tag){
		int firstAbility = -1;
		int tmpPriority;
		for (int i = 0, maxPriority = int.MinValue; i < ability.Count; i++) {
			tmpPriority = ability[i].GetComponent<ScriptGenericAbility>().Get_priority();
			if( HaveTag(ability[i],tag) && tmpPriority > maxPriority){
				firstAbility = i;
				maxPriority = tmpPriority;
			}
		}

		return firstAbility;
	}

}
#endregion



#region Abstract Class
//ogni effetto appartiene a questa classe astratta
public abstract class LinkedEffect : MonoBehaviour {
	//restituisce un valore che che dipende solo dall abilità stessa
	virtual public float Value (){ return 0; }						// valore fornito dall effetto
	//applica un abilità all oggetto o ne varia i parametri
	virtual public void ApplyEffect (GameObject targets){ }			// l'abilità stessa deve poter riconoscere il tipo di target, se un unità un abilità o altro
	virtual public void ApplyEffect (GameObject[] targets){ }
	virtual public void ApplyEffect (TacticMapNode targets) { }
	virtual public void ApplyEffect (TacticMapNode[] targets) { }
	virtual public void ApplyEffect (DamageRow targets){ }
	virtual public void ApplyEffect (DamageSend targets){ }
}

public abstract class ResourceCost : MonoBehaviour{
	virtual public ResourceCostType[] Get_ResourceCostType(){ return null; }
	virtual public float[] Get_ResourceCostValue(){ return null; }
}
#endregion


#region Classi Comuni
public class DamageRow{
	public int unityNumber {get; set;}
	public float baseDamage {get; set;}
	public AttackType attType {get; set;}
	public DamageType damType {get; set;}
	public List<GameObject> ability {get; set;}

	public DamageRow(int unityNumber, float baseDamage, AttackType attType, DamageType damType, List<GameObject> ability){
		this.unityNumber = unityNumber;
		this.baseDamage = baseDamage;
		this.attType = attType;
		this.damType = damType;
		this.ability = ability;
	}
}


// oggetto inviato quando si infligge un danno, contiene tutto le informazioni per permettere all BattleGroup che lo riceve di calcolare i danni
// i valori di questo oggetto non possono essere modificati dopo che viene allocato, chi lo riceve può solo leggerne i parametri
public class DamageSend{
	public float damage {get; set;} 
	public AttackType attType {get; set;} 
	public DamageType damType {get; set;} 
	public List<GameObject> ability {get; set;} 
	
	public DamageSend(float damage, AttackType attType, DamageType damType, List<GameObject> ability){
		this.damage = damage;
		this.attType = attType;
		this.damType = damType;
		this.ability = ability;
	}
}
#endregion




public class _ScriptGeneral : MonoBehaviour {

	void Start(){
		//MovementType asd = MovementType.fly;
	}

}
