using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using System;




public class GenericListOfChanges<T> where T : IComparable {
	protected List<T> changesList;

	public GenericListOfChanges (){
		changesList = new List<T> ();
	}

	// restituisce una COPIA di changesList
	public List<T> Get_List(){
		return CustomList<T>.Clone (changesList);
	}

	public void Add(T element){
		changesList.Add (element);
	}
	//aggiunge "element" in posizione "index"
	public void Add(T element, int index){
		changesList.Insert (index, element);
	}

	public void Remove(int index){
		changesList.RemoveAt (index);
	}
	//rimuove l'ultimo elemento
	public void Remove() {
		Remove (changesList.Count - 1);
	}
	//rimuove a partire da "index" "count" elementi
	public void Remove(int index, int count){
		changesList.RemoveRange (index, count);
	} 

}



#region Group Resource
public class NumberOfUnity {
	//public int a { get; private set; }
	static protected bool defaultSaveState = true;
	static protected int defaultMinNumber = 1;
	protected int maxNumber;
	protected int actNumber;
	public GenericListOfChanges<int> changesNumber;

	public NumberOfUnity(int number){
		if (!NumberValidate (number))
			maxNumber = defaultMinNumber;
		else
			maxNumber = number;
		actNumber = number;
		changesNumber = new GenericListOfChanges<int> ();
	}

	protected bool NumberValidate(int number){
		if (number >= defaultMinNumber)
			return true;
		else 
			return false;
	}

	/* n = valore che viene sottratto
	 * save = true --> salva la modifica del valore in una lista
	 * la sottrazione non può mandare actNumber sotto 0
	 */
	public void ModifyActNumber(int n, bool save){
		if (actNumber + n < 0)
			n -= actNumber + n;
		actNumber += n;
		if (save)
			changesNumber.Add(n);
	}
	public void ModifyActNumber(int n){
		ModifyActNumber (n, defaultSaveState);
	}

	#region Get
	public int Get_Number(){
		return actNumber;
	}

	public int Get_MaxNumber(){
		return maxNumber;
	}
	#endregion
}
#endregion






public class ScriptBattleGroup : MonoBehaviour {
	static protected float MIN_UNITY_HP = 0.1f;
	static protected int MIN_ARMOR = 0;
	static protected int MIN_DAMAGE = 0;

	static protected int THRESHOLD_MIN_INITIATIVE = -1000000000;
	static protected int THRESHOLD_MAX_INITIATIVE = 1000000000;
	static protected int FIX_INITIATIVE = 100000000;

	//TEST
	//abilità associate all oggetto DamageRow creato in Test()
	public List<GameObject> _abilityTestList;
	//END TEST

	[SerializeField] protected GameObject baseUnity;

	protected NumberOfUnity number;

	[SerializeField] protected float initiative;
	[SerializeField] protected float lastUnityHp;
	[SerializeField] protected bool death = false;
	[SerializeField] protected List<GameObject> abilityList;



	public void Constructor(/*GameObject baseU, */int quantity){
		//baseUnity = baseU;
		number = new NumberOfUnity (quantity);

		//inserire qui le funzioni che impostano il contenuto di abilityList
		//inserire qui le funzioni che impostano le altre variabili tenendo conto delle abilità dell unità


		lastUnityHp = CalculateEffectiveHealth (null);	//inserire le abilità prima di questa riga
		IsDeathSet ();

	}






	#region Public Method
	//funzione che viene richiamata dall esterno quando si subisce un attacco che causerà danni
	public void ReciveDamage(DamageRow damageReceive){
		//calcolo dei danni subiti, vengono calcolate parte delle abilità
		//Debug.Log ("Start Damage Send Creation");
		DamageSend tmpDam = new DamageSend ( CalculateTotalDamage(damageReceive), 
		                                    CalculateAttackType(damageReceive), 
		                                    CalculateDamageType(damageReceive), 
		                                    CalculateRemainingAbilityAfterRowCalc(damageReceive) );

		TakeDamage (tmpDam);
	}

	//DA IMPLEMENTARE
	public void ReceiveHeal(){
		/*
		 * permette al gruppo, a seconda dell abilità che la attiva, di:
		 * - ripristinare vita all ultima unità
		 * - resuscitare unità se il gruppo è vivo
		 * - resuscitare il gruppo se è morto
		 * - qualunque combinazione delle precedenti
		 */
	}

	//DA IMPLEMENTARE
	public bool UseAbility(){
		/*
		 * sono contate come abilità anche:
		 * - attacco base
		 * - movimento (1 o più caselle)
		 * - movimento + attacco
		 * - passare il turno senza compiere azioni
		 */
		if (CanUseAbility ()) {
			ModInitiative(0/* costo iniziativa abilità */);
			return true;
		} else {
			return false;
		}
	}

	
	
		#region Get Significative Value
	public bool IsDeath(){
		return death;
	}
	
	//numero attuale unità
	public int Get_ActNumber(){
		return number.Get_Number ();
	}
	//vita ultima unità
	public float Get_LastUnityHealth(){
		return lastUnityHp;
	}
	//vita singola unità
	public float Get_UnityHealth(){
		return CalculateEffectiveHealth (null);
	}
		#endregion

	#endregion



	#region Ability Function
	//DA IMPLEMENTARE
	//controlla che l'abilità selezionata sia utilizabile
	protected bool CanUseAbility(){
		return true;
	}
	
	//consuma l'iniziativa per l'utilizzo dell abilità
	protected void ConsumeInitiative(float mod){
		//applica le abilità che influiscono sul consumo dell iniziativa
		ModInitiative (mod);
	}

	protected void ModInitiative(float mod){
		initiative += mod;
		if (initiative > THRESHOLD_MAX_INITIATIVE)
			;//gestione eccezione iniziativa, abbassa l'iniziativa di tutte le unità in partita di FIX_INITIATIVE
		if (initiative < THRESHOLD_MIN_INITIATIVE)
			;//gestione eccezione iniziativa, alza l'iniziativa di tutte le unità in partita di FIX_INITIATIVE
	}
	#endregion



	#region Damage Take
	//legge l'oggetto di tipo DamageSend ricevuto e applica i danni subiti (scala il numero di unità e la vita dell ultima unità)
	protected void TakeDamage(DamageSend damageReceive){
		float tmpHp = CalculateEffectiveHealth (damageReceive);
		float tmpArmor = CalculateArmorContribution (damageReceive);
		damageReceive.damage = CalculateInflictDamage (damageReceive);
		float tmpHpAndArmor = tmpHp + tmpArmor;
		int tmpRowUnityKill = CalculateDeathUnityNumber (damageReceive, tmpHpAndArmor);

		ApplyUnityNumberReduction (damageReceive, tmpRowUnityKill);
		//imposta gli hp dell ultima unità e applica eventuali effetti
		ModifyLastUnityHealth (CalculateLastUnityHp (damageReceive, tmpHpAndArmor, tmpArmor, tmpRowUnityKill), tmpHp);

		ApplyOnDeath (damageReceive);

	}
	
		#region CALCOLO ABILITA'
	protected float CalculateApplyAbility(DamageRow damageReceive, EffectTagType[] tag){
		if (damageReceive != null) {
			float inc = CalculateAbility(damageReceive.ability,tag,true);
			//inc -= CalculateAbility(abilityList,tag,false);	//inserire qui il calcolo delle abilità dell unità che subisce l'attacco
			return inc;
		} else
			return 0;
	}
	//overload for DamageSend
	protected float CalculateApplyAbility(DamageSend damageReceive, EffectTagType[] tag){
		if (damageReceive != null) {
			float inc = CalculateAbility(damageReceive.ability,tag,true);
			//inc -= CalculateAbility(abilityList,tag,false);	//inserire qui il calcolo delle abilità dell unità che subisce l'attacco
			return inc;
		} else
			return 0;
	}
	//applica abilità in ability
	protected float CalculateAbility(List<GameObject> ability, EffectTagType[] tag,bool remove = false){
		int i = CustomAbilityList.FirstInList (ability, tag);
		float inc = 0;
		while (i >= 0) {
			switch (ability [i].GetComponent<ScriptGenericAbility> ().Get_EffectType ()) {
			case EffectType.value:
				inc += ability [i].GetComponent<ScriptGenericAbility> ().linkedScript.Value ();
				break;
			default:
				break;
			}
			//rimuove l'abilità dopo averla applicata
			if(remove)
				ability.RemoveAt (i);
			//seleziona la nuova abilità
			i = CustomAbilityList.FirstInList (ability, tag);
		}
		return inc;
	}
		#endregion

		#region Calculate Effective Stats (On Receive Damage)
			#region Calculate Damage Take
				#region Calculate Hp
	//calcolo vita
	protected float CalculateEffectiveHealth(DamageSend damageReceive){
		//qui verrà inserito il calcolo della vita effettiva del unità quando riceve l'attacco contando eventuali abilità
		float tmpHp = (CalculateBaseHealth (damageReceive) + CalculateHealthInc (damageReceive)) * (1 + CalculateHealthIncXC (damageReceive));

		return ValidateHealth (tmpHp);
	}
	//calcolo Hp Base
	protected float CalculateBaseHealth(DamageSend damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.baseHealth};
		return baseUnity.GetComponent<ScriptBaseUnity> ().health.Get_Value () +  CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//calcolo incremento fisso hp
	protected float CalculateHealthInc(DamageSend damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.healthInc};
		return CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//calcolo incremento percentuale hp
	protected float CalculateHealthIncXC(DamageSend damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.healthIncXC};
		return CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//trasforma gli hp in un valore sempre valido
	protected float ValidateHealth(float checkHp){
		if (checkHp > MIN_UNITY_HP)
			return checkHp;
		else
			return MIN_UNITY_HP;
	}
				#endregion

				#region Calculate Armor
	//calcolo armatura
	protected float CalculateArmorContribution(DamageSend damageReceive){
		//qui verrà inserito il calcolo del armor in base al tipo di armatura in relazione al tipo di danno
		float tmpArmor = ( (CalculateBaseArmor(damageReceive) + CalculateArmorInc(damageReceive))
		             		* (1 + CalculateArmorIncXC(damageReceive))
		             		* (1 + CalculateArmorTypeInc(damageReceive)) );

		return ValidateArmor (tmpArmor);
	}

	//calcolo armatura base
	protected float CalculateBaseArmor(DamageSend damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.baseArmor};
		return baseUnity.GetComponent<ScriptBaseUnity> ().armor.Get_Value ()  + CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//calcolo incremento fisso armatura
	protected float CalculateArmorInc(DamageSend damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.armorInc};
		return CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//calcolo incremento percentuale armatura
	protected float CalculateArmorIncXC(DamageSend damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.armorIncXC};
		return CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//calcola il contributo dell armatura in base al suo tipo e al tipo di danno ricevuto
	protected float CalculateArmorTypeInc(DamageSend damageReceive){
		/* valori > 0 aumentano l'armatura dell unità
		 * valori < 0 diminuiscono l'armatura dell unità
		 */
		float baseValue = 0;
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.armorTypeInc};
		return baseValue + CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//trasforma l'armatura in un valore sempre valido
	protected float ValidateArmor(float checkArmor){
		if (checkArmor > MIN_ARMOR)
			return checkArmor;
		else
			return MIN_ARMOR;
	}
				#endregion

	protected float CalculateInflictDamage(DamageSend damageReceive){
		/*applica eventuali abilità del unità che subisce l'attacco al danno, e le altre eventuali abilità dell attaccante
		 * es. abilità del unità che subisce l'attacco: riduce tutti i danni subiti di N per ogni unità attaccante
		 */
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.inflictDamage};
		return damageReceive.damage + CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//calcola il numero di unità uccise
	protected int CalculateDeathUnityNumber(DamageSend damageReceive, float totHpAndArmor){
		/*qui verranno calcolate le ultimissime abilità del difensore e dell attaccante che influenzano il numero di unità uccise
		 * es. abilità attaccante: l'attacco uccide almeno 1 unità
		 * es2. abilità attaccante/difensore: se un unità viene uccisa applica l'abilità X
		 */
		damageReceive.damage = CalculateFinalDamage (damageReceive);
		float tmpDeathNumber = damageReceive.damage / totHpAndArmor;
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.deathUnityNumber};
		return (int)(tmpDeathNumber + CalculateApplyAbility(damageReceive, tag));
	}//ABILITA' INSERITE

	//calcola i danni finali, richiamata immediatamente prima di calcolare le unità uccise
	protected float CalculateFinalDamage(DamageSend damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.finalDamage};
		return damageReceive.damage + CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	protected float CalculateLastUnityHp(DamageSend damageReceive, float tmpHpAndArmor, float tmpArmor, int tmpRowUnityKill) {
		//applica eventuali abilità che influenzano il danno subito dall ultima unità
		return -(damageReceive.damage - (tmpHpAndArmor * tmpRowUnityKill) - tmpArmor);
	}

	protected void ApplyUnityNumberReduction(DamageSend damageReceive, int reduction){
		GroupNumberReduction (-reduction);
		//applica eventuali funzioni che si attivano alla riduzione del numero di unità del gruppo bersaglio
	}

	protected void ApplyOnDeath(DamageSend damageReceive){
		IsDeathSet ();
		if (IsDeath()) {
			lastUnityHp = 0;
			
			;//applica eventuali funzioni alla morte dell gruppo bersaglio
		}
	}
			#endregion

			#region Calculate Damage Receive
	//calcolo numero unità attaccanti
	protected int CalculateAttackingUnityNumber(DamageRow damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.attackingUnityNumber};
		return damageReceive.unityNumber +  (int)CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE
				#region Calculate Damage
	//calcolo danni totali
	protected float CalculateTotalDamage(DamageRow damageReceive){
		float tmpDamage = (CalculateAttackingUnityNumber (damageReceive) 
		                   * (CalculateBaseDamage (damageReceive) + CalculateDamageInc (damageReceive))
		                   * (1 + CalculateDamageIncXC (damageReceive)) );
		// numero unità * danno base incrementato * incremento percentuale
		return ValidateDamage(tmpDamage);
	}

	//calcolo danni base ricevuti
	protected float CalculateBaseDamage(DamageRow damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.baseDamage};
		return damageReceive.baseDamage + CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//calcolo incremento fisso danni ricevuti
	protected float CalculateDamageInc(DamageRow damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.damageInc};
		return CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	//calcolo incremento percentuale danni ricevuti
	protected float CalculateDamageIncXC(DamageRow damageReceive){
		EffectTagType[] tag = new EffectTagType[] {EffectTagType.damageIncXC};
		return CalculateApplyAbility(damageReceive, tag);
	}//ABILITA' INSERITE

	protected float ValidateDamage(float tmpDamage){
		if (tmpDamage > MIN_DAMAGE)
			return tmpDamage;
		else
			return MIN_DAMAGE;
	}
				#endregion

	//calcolo tipo attacco
	protected AttackType CalculateAttackType(DamageRow damageReceive){
		return damageReceive.attType;
	}

	//calcolo tipo danno
	protected DamageType CalculateDamageType(DamageRow damageReceive){
		return damageReceive.damType;
	}

	//calcolo abilità rimanenti alla creazione dell oggetto DamageSend
	protected List<GameObject> CalculateRemainingAbilityAfterRowCalc(DamageRow damageReceive){
		/* elimina le abilità inutili o gia calcolate nei passaggi precedenti che non sono gia state eliminate in precedenza
		 * es. un abilità che fa calcoli in più funzioni non viene eliminata dalle rispettive funzioni, e viene quindi eliminata qui, se non serve per i calcoli successivi
		 * in generale comunque le funzioni Calculate... eliminano gia le abilità che utilizzano dalla lista in caso non vengano più utilizate
		 */
		return damageReceive.ability;
	}
			#endregion
		#endregion
	#endregion

	

	#region Health / death / NumberOfUnity Function
	//variazione numero unità
	protected void GroupNumberReduction(int mod){
		number.ModifyActNumber (mod);
	}


	protected bool IsDeathSet (){
		if (Get_ActNumber() <= 0)
			death = true;
		else
			death = false;
		return death;
	}

	//modifica la vita dell ultima unità
	protected void ModifyLastUnityHealth(float mod,float maxHp){
		float val = lastUnityHp + mod;
		if (val <= maxHp) {
			if (val >= 0) {
				lastUnityHp = val;
			} else
				ModifyLastUnityHealthUnder0 (mod,maxHp);
		} else
			ModifyLastUnityHealthOverMax (mod,maxHp);
	}

	//gestiscono i casi in cui la vita dell ultima unità ecceda il massimo o vada sotto 0
	protected void ModifyLastUnityHealthOverMax(float mod, float maxHp){
		lastUnityHp = maxHp;
	}
	protected void ModifyLastUnityHealthUnder0(float mod, float maxHp){
		lastUnityHp = 0;
	}
	#endregion




	public void Test(){
		/*Constructor (5);
		Debug.Log ("number --> "+Get_ActNumber());
		Debug.Log ("current hp --> "+Get_LastUnityHealth());
		Debug.Log ("is death? --> "+IsDeath());
		DamageRow t1 = new DamageRow (5, 10, 0, 0,_abilityTestList);
		ReciveDamage (t1);
		Debug.Log ("number --> "+Get_ActNumber());
		Debug.Log ("current hp --> "+Get_LastUnityHealth());
		Debug.Log ("is death? --> "+IsDeath());*/
	}




	public bool test=false,firstTime=true;
	public void Update(){
		if (test && firstTime) {
			Test ();
			firstTime = false;
		} else
			test = true;
	}
}
