﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;


#region TacticMapNode
public class TacticMapNode {
	private int coordX, coordY;
	private bool offsetToRight;
	private NodeType nodeType;
	private List<GameObject> abilityList;	//lista delle abilità presenti sul nodo

	// PATHFINDING VARIABLES 
	private TacticMapNode parent = null;
	private float fCost, gCost, hCost;
	private List<TacticMapNode> neighbours = new List<TacticMapNode>();
	

	
	public TacticMapNode(int x, int y, NodeType type) {
		coordX = x;
		coordY = y;
		nodeType = type;

		offsetToRight = (y%2 != 0);

	}
	
	public void UpdateNeighbours(TacticMapNode node) {
		neighbours.Add(node);
	}

	public int GetCoordX() {
		return coordX;
	}

	public int GetCoordY() {
		return coordY;
	}

	public List<TacticMapNode> GetNeighbours() {
		return neighbours;
	}

	public bool GetOffsetToRight() {
		return offsetToRight;
	}

	public bool IsOffset() {
		return offsetToRight;
	}	 
}
#endregion





#region ScriptTacticMap
public class ScriptTacticMap : MonoBehaviour {

	public static int[,] directions = {
		{-1,-1}, // Sinistra Alto
		{-1,0}, // Sinistra
		{-1,1}, // Basso Sinistra
		{0,1}, // Basso Destra
		{1,0}, // Destra
		{0,-1} // Alto Destra
	};

	public static int[,] directionsOffset = {
		{0,-1}, // Sinistra Alto
		{-1,0}, // Sinistra
		{0,1}, // Basso Sinistra
		{1,1}, // Basso Destra
		{1,0}, // Destra
		{1,-1} // Alto Destra
	};

	[SerializeField] private int sizeX = 0, sizeY = 0;
	private TacticMapNode[,] nodes;


	void Start() {

		int xx, yy;
		nodes = new TacticMapNode[sizeX,sizeY];

		for(int y = 0; y < sizeY; y++)
			for(int x = 0; x < sizeX; x++) 
				nodes[x,y] = new TacticMapNode(x, y, NodeType.none);

		for(int y = 0; y < sizeY; y++) {
			for(int x = 0; x < sizeX; x++) {
				for(int i = 0; i < 9; i ++) {
					xx = (int)Mathf.Floor(i%3)-1;
					yy = (int)Mathf.Floor(i/3)-1;

					if((xx == -1 && yy == 0) || (xx == 0 && yy != 0) || (xx == 1))
						if(IsBetween(x+xx, 0, sizeX-1) && IsBetween(y+yy, 0, sizeY-1)) // Check if between Map limits and add Neighbour
							nodes[x,y].UpdateNeighbours(nodes[x+xx, y+yy]);
				}
			}
		}

		#region TEST
		/*List<TacticMapNode> tmpTmp = GetNodesByShape(ShapeType.circle, 1, 3,3 , DirectionsType.right);
		foreach(TacticMapNode node in tmpTmp) {
			Debug.Log(node.GetCoordX() + " - " + node.GetCoordY());
		}*/
		#endregion
	} // End Start()



	#region GetCoordsByShape
	//overload node / x,y
	/*public List<int[]> GetCoordsByShape(ShapeType shape, int range, TacticMapNode node, DirectionsType direction = DirectionsType.right){
		if (node != null) 
			return GetCoordsByShape (shape, range, node.GetCoordX (), node.GetCoordY (), direction);
		else
			return null;
	}*/
	public List<int[]> GetCoordsByShape(ShapeType shape, int range, int x, int y, DirectionsType direction = DirectionsType.right) {

		List<int[]> tmpCoords = new List<int[]>();

		int currentX = x;
		int currentY = y;

		switch(shape) {

			case ShapeType.line:

				if(range == 0) return tmpCoords;

				for(int i = 0; i < range; i++) {

					if(IsOffset(currentY)) {
						currentX += directionsOffset[(int)direction,0];
						currentY += directionsOffset[(int)direction,1];
					} else {
						currentX += directions[(int)direction,0];
						currentY += directions[(int)direction,1];
					}
				
					tmpCoords.Add(new int[] {currentX,currentY});
					
				}

			break; // LINE


		case ShapeType.ring:

			if(range == 0) { tmpCoords.Add(new int[] {currentX,currentY}); return tmpCoords; }

			currentX += range;

			for(int i = 0; i < 6; i++) {
				tmpCoords.AddRange(GetCoordsByShape(ShapeType.line, range, currentX, currentY, (DirectionsType)i));
				currentX = tmpCoords[tmpCoords.Count-1][0];
				currentY = tmpCoords[tmpCoords.Count-1][1];
			}

			break; // RING


		case ShapeType.circle:

			if(range == 0) { tmpCoords.Add(new int[] {currentX,currentY}); return tmpCoords; }

			for(int i = 0; i<=range; i++) {
				tmpCoords.AddRange(GetCoordsByShape(ShapeType.ring, i, currentX, currentY));
			}

			break; // CIRCLE


		}
		
		return tmpCoords;

	}
	#endregion

	#region GetNodesByShape
	//overload node / x,y
	/*public List<TacticMapNode> GetNodesByShape(ShapeType shape, int range, TacticMapNode node, DirectionsType direction = DirectionsType.right){
		if (node != null) 
			return GetNodesByShape (shape, range, node.GetCoordX (), node.GetCoordY (), direction);
		else
			return null;
	}*/
	public List<TacticMapNode> GetNodesByShape(ShapeType shape, int range, int x, int y, DirectionsType direction = DirectionsType.right) {
		List<int[]> tmpCoords = GetCoordsByShape(shape, range, x, y, direction);
		List<TacticMapNode> tmpNodes = new List<TacticMapNode>();


		foreach(int[] tmp in tmpCoords) {
			if(IsBetween(tmp[0], 0, sizeX-1) && IsBetween(tmp[1], 0, sizeY-1))
				tmpNodes.Add(nodes[tmp[0],tmp[1]]);
		}

		return tmpNodes;

	}
	#endregion


	private static bool IsBetween(int value, int min, int max) {
		if(value >= min && value < max)
			return true;
		return false;
	}

	private static bool IsOffset(int y) {
		return y%2 != 0;
	}

}
#endregion
