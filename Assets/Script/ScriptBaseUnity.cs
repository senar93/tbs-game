using UnityEngine;
using System.Collections;

using System;






#region Base Resource
#region Generic Base Resource
public class BaseResource<T> where T : IComparable{
	protected T value;


	public BaseResource(){ }
	public BaseResource(T newValue){
		Set_Value (newValue);
	}

	protected void Set_Value(T newValue){
		value = newValue;
	}

	public T Get_Value(){
		return value;
	}
}

public class BaseResourceWithMin<T> : BaseResource<T> where T : IComparable{
	protected T minValue;

	//public BaseResourceWithMin(){ }
	public BaseResourceWithMin(T newValue, T newMinValue){
		Set_MinValue (newMinValue);
		Set_Value (newValue);
	}

	protected void Set_Value(T newValue){
		if (newValue.CompareTo(minValue) >= 0) 
			value = newValue;
		else {
			Debug.Log("ERROR INITIALIZE: Resource can not assume value " + newValue);
			value = minValue;
		}
	}

	protected void Set_MinValue(T newMinValue) {
		minValue = newMinValue;
	}

	public T Get_MinValue(){
		return minValue;
	}
}

public class BaseResourceWithRegen {
	// la rigenerazione si applicherà ogni volta che viene consumata dell iniziativa
	protected BaseResourceWithMin<int> value;
	protected BaseResource<float> regen;
	
	public BaseResourceWithRegen(int newValue, float newRegen){
		value = new BaseResourceWithMin<int> (newValue,0);
		regen = new BaseResource<float> (newRegen);
	}
	
	public int Get_Value(){
		return value.Get_Value();
	}
	
	public float Get_Regen(){
		return regen.Get_Value ();
	}
}
#endregion

#region Specific Resource
public class BaseInitiativeCost{
	protected BaseResourceWithMin<int> value;
	
	public BaseInitiativeCost(int initiative){
		value = new BaseResourceWithMin<int> (initiative,0);
	}
	
	public int Get_Value(){
		return value.Get_Value ();
	}
}


public class BaseHealth {
	protected BaseResourceWithMin<int> value;

	public BaseHealth(int newValue){
		value = new BaseResourceWithMin<int> (newValue,1);
	}

	public int Get_Value(){
		return value.Get_Value ();
	}
}


public class BaseArmor {

	protected BaseResourceWithMin<int> armor;
	protected ArmorType type;

	public BaseArmor(int value, ArmorType newType) {
		armor = new BaseResourceWithMin<int> (value, 0);
		type = newType;
	}

	public ArmorType Get_Type(){
		return type;
	}

	public int Get_Value(){
		return armor.Get_Value ();
	}
}


public class BaseAttack{

	protected BaseResourceWithMin<int> attack;
	protected BaseResourceWithMin<int> maxRangeValue;
	protected BaseInitiativeCost cost;
	protected AttackType attackType;
	protected DamageType damageType;

	public BaseAttack(int value, DamageType newDamageType, AttackType newAttackType, int newMaxRangeValue, int newInitiativeCost){
		attack = new BaseResourceWithMin<int> (value, 0);
		maxRangeValue = new BaseResourceWithMin<int> (newMaxRangeValue, 0);
		cost = new BaseInitiativeCost (newInitiativeCost);
		attackType = newAttackType;
		damageType = newDamageType;
	}

	public AttackType Get_AttackType(){
		return attackType;
	}

	public DamageType Get_DamageType(){
		return damageType;
	}

	public int Get_Value(){
		return attack.Get_Value ();
	}

	public int Get_MaxRange(){
		return maxRangeValue.Get_Value ();
	}

	public int Get_InitiativeCost(){
		return cost.Get_Value ();
	}
}


public class BaseMovement {

	// indica il numero massimo di caselle percorribili in un unico turno, indipendentemente da quanta iniziativa si vuole usare

	protected BaseResourceWithMin<int> maxRange;
	protected BaseInitiativeCost cost;
	protected MovementType type;

	public BaseMovement(int newValue, MovementType newType, int newInitiativeCost){
		maxRange = new BaseResourceWithMin<int> (newValue, 0);
		cost = new BaseInitiativeCost (newInitiativeCost);
		type = newType;
	}

	public MovementType Get_Type(){
		return type;
	}

	public int Get_Value(){
		return maxRange.Get_Value ();
	}

	public int Get_InitiativeCost(){
		return cost.Get_Value ();
	}
}


public class BaseAbilityList {
	protected GameObject[] ability;

	public BaseAbilityList(GameObject[] newAbility) {
		ability = newAbility;
	}

	public GameObject Get_Ability(int i){
		if (i < ability.Length && i > 0)
			return ability [i];
		else
			return null;
	}

	public GameObject[] Get_AllAbility(){
		return ability;
	}
}
#endregion
#endregion








public class ScriptBaseUnity : MonoBehaviour {
	public string dataSource = "ScriptBaseUnitySource";
	
	public BaseHealth health;
	public BaseResourceWithRegen mana;
	public BaseResourceWithRegen stamina;

	public BaseArmor armor;
	public BaseAttack attack;

	public BaseMovement speed;
	public BaseResource<int> initiative;

	public BaseAbilityList abilityList;


	public void LoadBaseUnity(){
		switch (dataSource) {
			case "ScriptBaseUnitySource":
			health = new BaseHealth(this.GetComponent<ScriptBaseUnitySource>().health);
			mana = new BaseResourceWithRegen(this.GetComponent<ScriptBaseUnitySource>().mana,
			                                 this.GetComponent<ScriptBaseUnitySource>().manaRegen);
			stamina = new BaseResourceWithRegen(this.GetComponent<ScriptBaseUnitySource>().stamina,
			                                    this.GetComponent<ScriptBaseUnitySource>().staminaRegen);
			armor = new BaseArmor(this.GetComponent<ScriptBaseUnitySource>().armor,
			                      this.GetComponent<ScriptBaseUnitySource>().armorType);
			attack = new BaseAttack(this.GetComponent<ScriptBaseUnitySource>().attackDamage,
			                        this.GetComponent<ScriptBaseUnitySource>().damageType,
			                        this.GetComponent<ScriptBaseUnitySource>().attackType,
			                        this.GetComponent<ScriptBaseUnitySource>().attackMaxRange,
			                        this.GetComponent<ScriptBaseUnitySource>().attackCostInitiative);
			speed = new BaseMovement(this.GetComponent<ScriptBaseUnitySource>().movementMaxRange,
			                         this.GetComponent<ScriptBaseUnitySource>().movementType,
			                         this.GetComponent<ScriptBaseUnitySource>().movementCost);
			initiative = new BaseResource<int>(this.GetComponent<ScriptBaseUnitySource>().initiative);
			abilityList = new BaseAbilityList(this.GetComponent<ScriptBaseUnitySource>().abilityList);
			break;

			default:
				break;
		}
	}



	public void Start(){
		LoadBaseUnity ();
	}


}
