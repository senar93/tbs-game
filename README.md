# README #
* Server Version: 0.0.1
* Client Version: none

# SERVER #
struttura :
 - The Maker *:  contiene valori, funzioni ecc globali all intero gioco*


 - Base *:  contiene le unità, le abilità, gli oggetti ecc di base del gioco, tutto quello che viene usato effettivamente in battaglia (tattica o strategica) viene prende i valori di base da questi oggetti (esempio, un gruppo di fanti avrà il collegamento all unità di base fante, oltre ai proprio parametri specifici di quella battaglia)*
   - Unity List
     - Unity <name> *:  contiene i valori di base dell unità, può prendere i dati da varie fonti (per ora solo da un altro script contenuto nello stesso oggetto)*
   - Talent List
     - Talent <name>
   - Item List
     - Item <name>
   - [...]


 - Tactic Games *:  contiene le battaglie tattiche*
   - Tactic Battle <id> *:  ogni battaglia tattica in corso sul server è un oggetto di questo tipo*
     - Map *:  mappa esagonale generata in maniera pseudocasuale o presa da mappe fisse precostruite a seconda delle impostazioni*
       - Cell <x><y>
     - Heroes
       - Hero <id>
       - [...]
     - Groups
       - Group <number> *:  il gruppo di unità effettivamente usate in partita, contiene un puntatore a un unità di base, e dei parametri propri del gruppo stesso (modificatori alle statistiche, abilità aggiuntive, posizione ecc)*